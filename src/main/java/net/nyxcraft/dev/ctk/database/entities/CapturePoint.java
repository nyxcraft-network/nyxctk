package net.nyxcraft.dev.ctk.database.entities;

/**
 * Created by Gert-Jan on 26-12-2014.
 */

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity(value = "capture_point", noClassnameStored = true)
public class CapturePoint {

    @Id
    public ObjectId id;

    public String name;
    public String centerBlock;
    public String spawnBlock;
    public String clickBlock;
    public String teamName;
    public int maxHP;

    public CapturePoint(String name, String centerBlock, String spawnBlock, String clickBlock, String teamName, int maxHP) {
        this.name = name;
        this.centerBlock = centerBlock;
        this.spawnBlock = spawnBlock;
        this.clickBlock = clickBlock;
        this.teamName = teamName;
        this.maxHP = maxHP;
    }
}
