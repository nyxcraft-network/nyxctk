package net.nyxcraft.dev.ctk.database.api;

import net.nyxcraft.dev.ctk.database.dao.CapturePointDAO;
import net.nyxcraft.dev.ctk.database.entities.CapturePoint;
import org.bson.types.ObjectId;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.mongodb.morphia.query.UpdateResults;

public class CapturePointAPI {

    public static void create(CapturePoint capturePoint) { CapturePointDAO.getInstance().save(capturePoint); }

    private static Query<CapturePoint> createQueryFromId(ObjectId id) {
        return CapturePointDAO.getInstance().createQuery().field("id").equal(id);
    }

    private static UpdateResults<CapturePoint> update(ObjectId id, UpdateOperations<CapturePoint> ops) {
        return CapturePointDAO.getInstance().update(createQueryFromId(id), ops);
    }
}
