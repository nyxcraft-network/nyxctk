package net.nyxcraft.dev.ctk.database.dao;

import net.nyxcraft.dev.ctk.database.entities.CapturePoint;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

/**
 * Created by Gert-Jan on 26-12-2014.
 */
public class CapturePointDAO extends BasicDAO<CapturePoint, ObjectId> {
    private static CapturePointDAO instance;

    public CapturePointDAO(Datastore ds) {
        super(ds);
        instance = this;
    }

    public static CapturePointDAO getInstance() { return instance; }
}
