package net.nyxcraft.dev.ctk.commands;

import net.nyxcraft.dev.ctk.CTKPlugin;
import net.nyxcraft.dev.ctk.arena.CapturePoint_old;
import net.nyxcraft.dev.ctk.arena.Team;
import net.nyxcraft.dev.nyxcore.command.CommandRegistry;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

/**
 * Created by Gert-Jan on 14-12-2014.
 */
public class AdminCommands {

    public AdminCommands () {
        CommandRegistry.registerPlayerCommand(CTKPlugin.getInstance(), "ctk", AdminCommands::ctkCommand);
        CommandRegistry.registerPlayerSubCommand("ctk", "setflag", AdminCommands::setFlagCommand);
        CommandRegistry.registerPlayerSubCommand("ctk", "setspawn", AdminCommands::setSpawnCommand);
    }


    public static void ctkCommand (Player player, String args[]) {
        AdminCommands.ctkHelp(player, args);
    }

    public static void setFlagCommand (Player player, String args[]) {
        Block block = player.getTargetBlock(null, 200);

        if(block == null) {
            player.sendMessage(ChatColor.RED+"You aren't looking at a block!");
            return;
        }

        if(args[0].equalsIgnoreCase("red")){
            new CapturePoint_old(CTKPlugin.getInstance(), "Point", block.getLocation(), Team.RED);
        } else if(args[0].equalsIgnoreCase("blue")) {
            new CapturePoint_old(CTKPlugin.getInstance(), "Point", block.getLocation(), Team.BLUE);
        } else {
            player.sendMessage("Use 'red' or 'blue' to define the a flag for a team.");
        }
    }

    public static void setSpawnCommand (Player player, String args[]) {

    }

    public static void ctkHelp (Player player, String arg[]) {
        player.sendMessage("=== ### CTK Admin Help ### ===");
        player.sendMessage("/ctk setflag [teamname]");
        player.sendMessage("/ctk setspawn [teamname]");
        player.sendMessage("/ctk start");
        player.sendMessage("/ctk stop");
        player.sendMessage("/ctk reset");
    }


}
