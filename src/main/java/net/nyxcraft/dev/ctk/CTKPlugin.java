package net.nyxcraft.dev.ctk;

import net.nyxcraft.dev.ctk.arena.CapturePoint_old;
import net.nyxcraft.dev.ctk.arena.Team;
import net.nyxcraft.dev.ctk.commands.AdminCommands;
import net.nyxcraft.dev.ctk.database.entities.CapturePoint;
import net.nyxcraft.dev.ctk.listeners.EventListeners;
import net.nyxcraft.dev.ctk.managers.LobbyManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Gert-Jan on 2-12-2014.
 */
public class CTKPlugin extends JavaPlugin {
    private static CTKPlugin instance;
    private Logger log = Logger.getLogger("Minecraft");

    public ArrayList<Player> teamBlue = new ArrayList<> ();
    public ArrayList<Player> teamRed = new ArrayList<> ();

    public HashMap<String, CapturePoint_old> capturePoints = new HashMap<String, CapturePoint_old>();

    private static String prefix = ChatColor.DARK_GRAY+"["+ChatColor.AQUA+"CTK"+ChatColor.DARK_GRAY+"] "+ChatColor.WHITE;

    public void onEnable() {
        instance = this;
        init();

        //Start the lobby on plugin start
        new LobbyManager(this);

        //Test points
        CapturePoint_old cp_red1 = new CapturePoint_old(this, "Test_Red_1", new Location(Bukkit.getWorlds().get(0), 91, 4, -132), Team.RED);
        this.capturePoints.put("test_r1", cp_red1);

        CapturePoint_old cp_red2 = new CapturePoint_old(this, "Test_Red_2", new Location(Bukkit.getWorlds().get(0), 91, 4, -147), Team.RED);
        this.capturePoints.put("test_r2", cp_red2);

        CapturePoint_old cp_blue1 = new CapturePoint_old(this, "Test_Blue_1", new Location(Bukkit.getWorlds().get(0), 76, 4, -132), Team.BLUE);
        this.capturePoints.put("test_b1", cp_blue1);

        CapturePoint_old cp_nlue2 = new CapturePoint_old(this, "Test_Blue_2", new Location(Bukkit.getWorlds().get(0), 76, 4, -147), Team.BLUE);
        this.capturePoints.put("test_b2", cp_nlue2);
    }

    public static CTKPlugin getInstance(){
        return instance;
    }

    public void init(){
        registerListeners();
        registerCommands();
    }

    public void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new EventListeners(this), this);
    }

    public void registerCommands() {
        new AdminCommands();
    }

    public void broadcastMessage (String message) {
        Bukkit.broadcastMessage(message);
    }
    public void sendMessage(Player player, String message) { player.sendMessage(getPrefix()+message); }

    public static String getPrefix() {
        return prefix;
    }

    public Logger getLog() {
        return log;
    }

    public void logError(String message) {
        getLog().log(Level.SEVERE, message);
    }

    public void logInfo(String message) {
        getLog().log(Level.INFO, message);
    }
}
