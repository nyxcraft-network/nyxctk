package net.nyxcraft.dev.ctk.arena;

import net.nyxcraft.dev.ctk.CTKPlugin;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

/**
 * Created by Gert-Jan on 14-12-2014.
 */
public class CapturePoint_old {

    private final CTKPlugin plugin;

    private String name;
    private Location centerBlock;
    private Location flagCenterBlock;
    private Team team;

    private Integer currentHP;
    private Integer MaxHP = 10;

    public CapturePoint_old(CTKPlugin plugin, String name, Location centerBlock, Team team) {
        this.plugin = plugin;

        this.name = name;
        this.centerBlock = centerBlock;
        this.flagCenterBlock = centerBlock.clone().add(0, 8, 0);
        this.team = team;
        this.currentHP = this.getMaxHP();

        this.makeFlag();
    }

    public String getName() {
        return name;
    }

    public Location getCenterBlock() {
        return centerBlock;
    }

    public Team getTeam() {
        return team;
    }

    public Integer getCurrentHP() {
        return currentHP;
    }

    public Integer getMaxHP() {
        return MaxHP;
    }

    public void makeFlag() {
        DyeColor color = DyeColor.BLUE;
        if (this.team == Team.RED) {
            color = DyeColor.RED;
        }

        Block wool = flagCenterBlock.getBlock();
        wool.setTypeIdAndData(35, color.getData(), true);
        wool.getRelative(BlockFace.NORTH).setTypeIdAndData(35, color.getData(), true);
        wool.getRelative(BlockFace.SOUTH).setTypeIdAndData(35, color.getData(), true);
        wool.getRelative(BlockFace.EAST).setTypeIdAndData(35, color.getData(), true);
        wool.getRelative(BlockFace.WEST).setTypeIdAndData(35, color.getData(), true);
        wool.getRelative(BlockFace.UP).setTypeIdAndData(35, color.getData(), true);
        wool.getRelative(BlockFace.DOWN).setTypeIdAndData(35, color.getData(), true);
    }

    public void captureFlag(Team team) {
        Block cp = this.flagCenterBlock.clone().add(0, 2, 0).getBlock(); //Move the center up by 2 so we spawn fireworks a above the flag.
        this.team = team;

        ChatColor color = ChatColor.BLUE;
        Color fireworkColor = Color.BLUE;

        if(team == team.RED) {
            color = ChatColor.RED;
            fireworkColor = Color.RED;
        }

        //Spawn Firework
        Firework firework = cp.getWorld().spawn(cp.getLocation(), Firework.class);
        FireworkMeta data = firework.getFireworkMeta();
        data.addEffect(FireworkEffect.builder().withColor(fireworkColor).with(FireworkEffect.Type.BALL_LARGE).build());
        data.setPower(1);
        firework.setFireworkMeta(data);

        this.makeFlag();

        CTKPlugin.getInstance().broadcastMessage(CTKPlugin.getPrefix()+ChatColor.GREEN + this.name + ChatColor.YELLOW+ " has just been captured by the "+ color + this.team.name() + " team!");
    }

    public void nearPoint(Player player) {
        Team playerTeam = this.getPlayerTeam(player);

        //If we own the CP
        if(this.team.equals(playerTeam)) {
            if(this.currentHP < this.getMaxHP()) {
                this.currentHP++;
                player.playSound(this.getCenterBlock(), Sound.IRONGOLEM_HIT, 5, 0);
                plugin.sendMessage(player, ChatColor.YELLOW+"Re-capture progress: "+this.currentHP+"/"+this.getMaxHP());
            }
        } else {
            if(this.currentHP > 0) {
                this.currentHP--;
                if(this.currentHP == this.getMaxHP() - 5){
                    plugin.broadcastMessage(CTKPlugin.getPrefix()+ChatColor.RED + this.name + " is being attacked by the " + playerTeam.name() + " team!");
                }

                player.playSound(this.getCenterBlock(), Sound.IRONGOLEM_HIT, 5, 0);
                plugin.sendMessage(player, "Capture progress: "+this.currentHP+"/"+this.getMaxHP());
            } else {
                this.currentHP = this.getMaxHP();
                if(this.team == Team.BLUE) {
                    this.team = Team.RED;
                } else {
                    this.team = Team.BLUE;
                }

                //Give the capturing team points / Remove the losing team points
                this.captureFlag(this.team);
            }
        }
    }

    public Team getPlayerTeam(Player player) {
        if(plugin.teamBlue.contains(player)) {
            return Team.BLUE;
        } else {
            return Team.RED;
        }
    }
}
