package net.nyxcraft.dev.ctk.arena;

import org.bukkit.Color;

/**
 * Created by Gert-Jan on 14-12-2014.
 */
public enum Team {
    RED ("Red", Color.RED),
    BLUE ("Blue", Color.BLUE);

    private final String teamName;
    private final Color color;

    Team(String teamName, Color color) {
        this.teamName = teamName;
        this.color = color;
    }

    private String teamName() { return teamName; }
    private Color  color() { return color; }
}
