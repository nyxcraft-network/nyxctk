package net.nyxcraft.dev.ctk.arena;

import org.bukkit.Location;

/**
 * Created by Gert-Jan on 9-12-2014.
 */
public class Arena {
    private String name;
    private String author;
    Location blueSpawn;
    Location redSpawn;


    public Arena(String name, String author, Location blueSpawn, Location redSpawn) {
        this.setName(name);
        this.setAuthor(author);
        this.setBlueSpawn(blueSpawn);
        this.setRedSpawn(redSpawn);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Location getBlueSpawn() {
        return blueSpawn;
    }

    public void setBlueSpawn(Location blueSpawn) {
        this.blueSpawn = blueSpawn;
    }

    public Location getRedSpawn() {
        return redSpawn;
    }

    public void setRedSpawn(Location redSpawn) {
        this.redSpawn = redSpawn;
    }
}
