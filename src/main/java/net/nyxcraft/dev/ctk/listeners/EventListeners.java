package net.nyxcraft.dev.ctk.listeners;

import net.nyxcraft.dev.ctk.CTKPlugin;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Gert-Jan on 9-12-2014.
 */
public class EventListeners implements Listener {

    private final CTKPlugin plugin;

    public EventListeners(CTKPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
        Player player = event.getPlayer();

        //Add player to team
        if(plugin.teamRed.size() < plugin.teamBlue.size()) {
            plugin.teamRed.add(player);
            player.sendMessage("Team red");
        } else {
            plugin.teamBlue.add(player);
            player.sendMessage("Team blue");
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        this.removePlayerFromTeam(event.getPlayer());
    }

    @EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
        this.removePlayerFromTeam(event.getPlayer());
    }

    /**
     * Do some cleanup when the player leaves the game one way or another.
     * @param player
     */
    private void removePlayerFromTeam(Player player){
        if(plugin.teamBlue.contains(player)){
            plugin.teamBlue.remove(player);
        } else {
            plugin.teamRed.remove(player);
        }
    }


    @EventHandler
    public void onEntityExplode (EntityExplodeEvent event) {
        for(Block block : event.blockList()){
            block.setType(Material.AIR);
        }
    }

    @EventHandler
    public void onBlockPlace (BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if(player.isOp()){
            return;
        }

        if(event.getItemInHand().getType().equals(Material.WOOL)){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onCraftItem (CraftItemEvent event) {
        event.getInventory().clear();
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerItemPickup(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onArmorInteract(InventoryClickEvent event){
        if (event.getSlotType().equals(InventoryType.SlotType.ARMOR))
            event.setCancelled(true);
    }
}
