package net.nyxcraft.dev.ctk;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gert-Jan on 13-12-2014.
 */
public class Util {


    public static List<Player> playersNearPoint(Location location) {
        List<Player> players = new ArrayList<Player>();
        for(Player player : CTKPlugin.getInstance().getServer().getOnlinePlayers()) {
            if(location.distance(player.getLocation()) < 6) {
                players.add(player);
            }
        }

        return players;
    }
}
