package net.nyxcraft.dev.ctk.managers;

import net.nyxcraft.dev.ctk.CTKPlugin;
import org.bukkit.ChatColor;

/**
 * Created by Gert-Jan on 13-12-2014.
 */
public class LobbyManager {

    private final CTKPlugin plugin;
    private int lobbyDelay;
    private int lobbyTicker;

    public LobbyManager(CTKPlugin plugin) {
        this.plugin = plugin;

        startLobby();
    }

    private void startLobby() {
        lobbyDelay = 30;
        lobbyTicker = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, getLobbyRunnable(), 0L, 20L);
    }

    private void endLobby () {
        plugin.getServer().getScheduler().cancelTask(lobbyTicker);

        new GameManager(plugin);
        //DEBUG
//        if(plugin.teamBlue.size() > 5 && plugin.teamRed.size() > 5) {
//            new GameManager(plugin);
//        } else {
//            plugin.broadcastMessage(CTKPlugin.getPrefix()+ChatColor.RED+"Not enough players to start a game. Restarting lobby");
//            startLobby();
//        }
    }

    private void doTick() {
        switch(lobbyDelay--){
            case 30:
            case 20:
            case 10:
                plugin.broadcastMessage(CTKPlugin.getPrefix()+ ChatColor.YELLOW+"The next round starts in "+(lobbyDelay+1)+" seconds!");
                break;
            case 0:
                endLobby();
                break;
            default:
                break;
        }
    }

    public Runnable getLobbyRunnable() {
        return () -> doTick();
    }
}
