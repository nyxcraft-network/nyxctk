package net.nyxcraft.dev.ctk.managers;

import net.nyxcraft.dev.ctk.CTKPlugin;
import net.nyxcraft.dev.ctk.Util;
import net.nyxcraft.dev.ctk.arena.CapturePoint_old;
import net.nyxcraft.dev.ctk.arena.Team;
import net.nyxcraft.dev.ctk.database.entities.CapturePoint;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gert-Jan on 9-12-2014.
 */
public class GameManager {

    private CTKPlugin plugin;
    private static GameManager instance;

    private int ticks;
    private int gameTicker;
    private int captureTicker;

    public HashMap<Team, ArrayList<CapturePoint_old>> capturedPoints = new HashMap<Team, ArrayList<CapturePoint_old>>();

    public GameManager (CTKPlugin plugin) {
        this.plugin = plugin;
        instance = this;

        //1 minute round (DEBUG)
        ticks = 60 * 1;

        plugin.broadcastMessage(CTKPlugin.getPrefix()+ChatColor.GOLD+"A new round of Capture the Keep has started");
        plugin.broadcastMessage(CTKPlugin.getPrefix()+ChatColor.GOLD+"Pick your spawnpoint by right-clicking a colored wool");
        plugin.broadcastMessage("");

        //Schedule events to keep the game running
        gameTicker = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, getGameRunnable(), 0L, 20L);
        captureTicker = plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, getCaptureRunnable(), 0L, 20L);

        this.capturedPoints.put(Team.RED, new ArrayList<>());
        this.capturedPoints.put(Team.BLUE, new ArrayList<>());
    }

    //Runs every second (20 mc ticks)
    private void doGameTick() {
        if(ticks > 0){
            if(plugin.teamBlue.size() == 0 || plugin.teamRed.size() == 0) {
//                endGame();
            }

            //Keep the players informed of the gametime they have left.
            if(ticks == 1200) {
                plugin.broadcastMessage(CTKPlugin.getPrefix()+ChatColor.GREEN+"20 minutes left this round");
            }

            if(ticks == 600) {
                plugin.broadcastMessage(CTKPlugin.getPrefix()+ChatColor.GREEN+"10 minutes left this round");
            }

            if(ticks == 300) {
                plugin.broadcastMessage(CTKPlugin.getPrefix()+ChatColor.GREEN+"5 minutes left this round");
            }

            if(ticks == 60){
                plugin.broadcastMessage(CTKPlugin.getPrefix()+ChatColor.GREEN+"1 minute left");
            }

            ticks--;
        } else {
            endGame();
        }
    }

    private void doCaptureTick() {
        System.out.println("Capture tick");
        for(Map.Entry<String, CapturePoint_old> entry : plugin.capturePoints.entrySet()){
            for (Player player : Util.playersNearPoint(entry.getValue().getCenterBlock())){
                entry.getValue().nearPoint(player);
            }
        }
    }


    public void endGame() {
        plugin.getServer().getScheduler().cancelTask(gameTicker);
        plugin.getServer().getScheduler().cancelTask(captureTicker);

        if(capturedPoints.containsKey(Team.RED) && capturedPoints.get(Team.RED).size() == 0) {
            //Team blue captured all the points and won
            plugin.broadcastMessage(ChatColor.YELLOW+"Blue team has captured all the points and won the game!");
            //TODO add x points for winning players (blue)
        } else if(capturedPoints.containsKey(Team.BLUE) && capturedPoints.get(Team.BLUE).size() == 0) {
            //Team blue captured all the points and won
            plugin.broadcastMessage(ChatColor.YELLOW+"Red team has captured all the points and won the game!");
            //TODO add x points for winning players (red)
        } else {
            plugin.broadcastMessage(ChatColor.YELLOW+"The round ran out of time");

            if(capturedPoints.get(Team.RED).size() > capturedPoints.get(Team.BLUE).size()) {
                //Team red has most
                plugin.broadcastMessage("Team red won! They had: "+ capturedPoints.get(Team.RED).size()+" points captured!");
                //TODO add x/2 for winning players (red)
            } else if(capturedPoints.get(Team.RED).size() < capturedPoints.get(Team.BLUE).size()){
                //Team blue has most
                plugin.broadcastMessage("Team blue won! They had: "+ capturedPoints.get(Team.RED).size()+" points captured!");
                //TODO add x/2 for winning players (blue)
            } else {
                plugin.broadcastMessage("The game tied!");
            }
        }

        instance = null;

        for(Player player : plugin.getServer().getOnlinePlayers()){
            player.playSound(player.getLocation(), Sound.NOTE_PIANO, 5, 0);
            player.playSound(player.getLocation(), Sound.NOTE_PIANO, 5, 1);
            player.playSound(player.getLocation(), Sound.NOTE_PIANO, 5, 2);
            player.playSound(player.getLocation(), Sound.NOTE_PIANO, 5, 3);
        }

        plugin.broadcastMessage("");
        plugin.broadcastMessage("The round is over, you will be teleported to the lobby. A new game will start shortly.");
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, getSendLobbyRunnable(), 200L);
    }

    /// ### Runnables ### ///
    private Runnable getGameRunnable() {
        return () -> doGameTick();
    }

    private Runnable getCaptureRunnable(){
        return () -> doCaptureTick();
    }

    private Runnable getSendLobbyRunnable(){
        return new Runnable(){
            public void run(){
                for(Player player : plugin.getServer().getOnlinePlayers()){
                    //TODO SEND TO LOBBY!
//                    plugin.util.sendtoLobby(player);
                }

                new LobbyManager(plugin);
            }
        };
    }
}
